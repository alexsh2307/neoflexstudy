package ru.dbapp.demo.db;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarsRep extends CrudRepository<Cars, Long> {
}
